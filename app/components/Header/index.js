import React from 'react';
import { FormattedMessage } from 'react-intl';

import A from './A';
import Img from './Img';
import NavBar from './NavBar';
import HeaderLink from './HeaderLink';
import LoginLink from './LoginLink';
import messages from './messages';

function Header() {
  return (
    <div>
      <h1>ACBay</h1>
      <NavBar>
        <HeaderLink to="/">
          <FormattedMessage {...messages.home} />
        </HeaderLink>
        <LoginLink />
      </NavBar>
    </div>
  );
}

export default Header;
