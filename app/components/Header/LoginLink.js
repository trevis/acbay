import React from 'react';
import NormalA from 'components/A';

function LoginLink(props) {
  return (
    <NormalA href="/login/discord">
      Login with discord
    </NormalA>
  );
}

export default LoginLink;