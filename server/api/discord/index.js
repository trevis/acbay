const path = require('path');
const express = require('express');
const fetch = require('node-fetch');
const btoa = require('btoa');
const { catchAsync } = require('../../utils');

module.exports = function addDiscordMiddlewares(app, options) {
  app.get('/login/discord', (req, res) => {
    res.redirect(`https://discordapp.com/oauth2/authorize?client_id=${options.clientId}&scope=identify&response_type=code&redirect_uri=${options.clientRedirect}`);
  });

  app.get('/api/discord/callback', catchAsync(async (req, res) => {
    if (!req.query.code) throw new Error('NoCodeProvided');

    const code = req.query.code;
    const creds = btoa(`${options.clientId}:${options.clientSecret}`);

    const response = await fetch(`https://discordapp.com/api/oauth2/token?grant_type=authorization_code&code=${code}&redirect_uri=${options.clientRedirect}`,
        {
        method: 'POST',
        headers: {
            Authorization: `Basic ${creds}`,
        },
    });

    const json = await response.json();
    

    fetch('http://discordapp.com/api/users/@me', { 
      method: 'get', 
      headers: {
        'Authorization': 'Bearer ' + json.access_token
      }
    })
    .then(function(response) {
      return response.json();
    })
    .then(function(userJson) {
      //res.send(userJson);
      if (userJson && userJson.username) {
        res.send(`HELLO ${userJson.username}`);
      }
      else {
        res.send(':(');
      }
    });

    //res.redirect(`/?token=${json.access_token}`);
  }));

  return app;
};
