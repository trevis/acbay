/* eslint-disable global-require */

/**
 * API middleware
 */
module.exports = (app, options) => {
    const addDiscordMiddlewares = require('./discord');
    addDiscordMiddlewares(app, options);

    return app;
};
  